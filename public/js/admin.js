try {
    window.$ = window.jQuery = require('jquery');
} catch (e) {}

/* Set the width of the sidebar to 250px and the left margin of the page content to 250px */
function openNav() {
    $("#sidebar").width("250px");
    $("#main").css("marginLeft", "250px");
    $("#sidebar").css("top", $(".navbar.navbar-expand-md").outerHeight());
}
  
/* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
function closeNav() {
    $("#sidebar").width(0);
    $("#main").css("marginLeft", "0");
}