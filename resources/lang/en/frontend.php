<?php

return [

    /**
     * English text for frontend content
     * 
     * @author Chithra<cct@lbit.in>
     * 
     * @return array
     * 
     */

    'prof.feedback' => 'Professional Feedback',
    'personal.feedback' => 'Personal Feedback',
    'input_error' => 'Enter correct values.',
    'field_error' => 'Enter :Name',
    'feedback.save' => 'Save'
];