<?php

return [

    /**
     * English text for admin content
     * 
     * @author Chithra<cct@lbit.in>
     * 
     * @return array
     * 
     */

    'name' => 'Name',
    'slug' => 'Slug',
    'company' => 'Company',
    'company_name' => 'Company Name',
    'company_slug' => 'Company Slug',
    'new_company' => 'Add New Company',
    'back' => 'Back', 
    'input_error' => 'Enter correct values.',
    'field_error' => 'Enter :Name',
    'submit' => 'Submit',
    'companies_title' => 'Companies',
    'actions' => 'Actions',
    'delete' => 'Delete',
    'si_no' => 'No.',
    'show' => 'Show',
    'edit' => 'Edit',
    'company_success' => 'Company has been added.',
    'edit_company' => 'Update Company',
    'company_update' => 'Company updated successfully.',
    'company_delete' => 'Company deleted successfully.',
    'manage_company' => 'Manage Company',
    'companies_add' => 'Add Company',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'menu' => 'Menu',
    'email' => 'Email',
    'users_title' => 'Users',
    'users_add' => 'Add User',
    'new_user' => 'New User',
    'user_success' => 'User has been added.',
    'edit_user' => 'Update User',
    'user_update' => 'User updated successfully.',
    'user_delete' => 'User deleted successfully.',
    'manage_user' => 'Manage Users',
    'photo' => 'Photo',
    'select_company' => 'Select Company',
    'dashboard' => 'Dashboard',
    'welcome' => 'Welcome!'
];