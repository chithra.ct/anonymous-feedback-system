@extends('layouts.app')

@section('content')
    <div class="card-body">
        <div class="row">
            @foreach ($users as $user)
                <div class="col-sm-3 company_users">
                    <div class="card">
                        @if ($user->photo)
                            <img src="{{asset('/'.config('feedback.path').'/'.$user->photo)}}" alt="{{ $user->photo }}" />
                        @endif
                        <div class="card-body">
                            <p class="card-text text-center">
                                <a href="{{ route('user', $user) }}">
                                    {{ $user->name }}
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection