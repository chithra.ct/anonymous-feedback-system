@extends('layouts.app')
@section('content')
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3 company_users">
                <div class="card">
                    @if ($user->photo)
                        <img src="{{asset('/'.config('feedback.path').'/'.$user->photo)}}" alt="{{ $user->photo }}" />
                    @endif
                    <div class="card-body">
                        <p class="card-text text-center">
                            {{ $user->name }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 company_users">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        @lang('frontend.input_error')
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('feedback') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="user_name">@lang('frontend.prof.feedback')</label>
                        <textarea class="form-control" id="prof_feedback" placeholder="@lang('admin.field_error', ['Name' => 'feedback'])" name="prof_feedback">{{ old('prof_feedback') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="user_name">@lang('frontend.personal.feedback')</label>
                        <textarea class="form-control" id="prof_feedback" placeholder="@lang('admin.field_error', ['Name' => 'feedback'])" name="personal_feedback">{{ old('personal_feedback') }}</textarea>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{ $user->id }}">
                        <input type="hidden" class="form-control" id="user_email" name="user_email" value="{{ $user->email }}">
                    </div>
                    <button type="submit" class="btn btn-primary">@lang('frontend.feedback.save')</button>
                </form>
            </div>
        </div>
    </div>
@endsection