@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.users_title')
                <a class="btn btn-success float-right add_company" href="{{ route('users.index') }}">
                    @lang('admin.back')
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>@lang('admin.name'):</th>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <th>@lang('admin.email'):</th>
                <td>{{ $user->email }}</td>
            </tr>
            <tr>
                <th>@lang('admin.company'):</th>
                <td>
                    @if ($company = $user->company)
                        {{ $company->name }}
                    @endif
                </td>
            </tr>
            <tr>
                <th>@lang('admin.photo'):</th>
                <td>
                    <img class="user_photo" src="{{asset('/'.config('feedback.path').'/'.$user->photo)}}" alt="{{ $user->photo }}">
                </td>
            </tr>
        </table>
    </div>
@endsection