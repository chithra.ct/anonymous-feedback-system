@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.users_title')
                <a class="btn btn-success float-right add_company" href="{{ route('users.create') }}">
                    @lang('admin.users_add')
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <table class="table table-bordered">
            <tr>
                <th>@lang('admin.si_no')</th>
                <th>@lang('admin.name')</th>
                <th>@lang('admin.email')</th>
                <th>@lang('admin.company')</th>
                <th>@lang('admin.photo')</th>
                <th>@lang('admin.actions')</th>
            </tr>
            @php
                $i = 0;
            @endphp
            @foreach ($users as $user)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @if ($company = $user->company)
                            {{ $company->name }}
                        @endif
                    </td>
                    <td>
                        @if ($user->photo)
                            <img class="user_photo" src="{{asset('/'.config('feedback.path').'/'.$user->photo)}}" alt="{{ $user->photo }}" />
                        @endif
                    </td>
                    <td>
                        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('users.show', $user->id) }}">@lang('admin.show')</a>
                            <a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}">@lang('admin.edit')</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">@lang('admin.delete')</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $users->links() !!}
    </div>
@endsection