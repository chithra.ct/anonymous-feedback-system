@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.new_user')
                <a class="btn btn-success float-right add_company" href="{{ route('users.index') }}">
                    @lang('admin.back')
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                @lang('admin.input_error')
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('users.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="user_name">@lang('admin.name')</label>
                <input type="text" class="form-control" id="user_name" placeholder="@lang('admin.field_error', ['name' => 'name'])" name="name" value="{{ old('name') }}">
            </div>
            <div class="form-group">
                <label for="user_email">@lang('admin.email')</label>
                <input type="text" class="form-control" id="user_email" placeholder="@lang('admin.field_error', ['name' => 'email'])" name="email" value="{{ old('email') }}">
                <input type="hidden" name="password" value="12345">
            </div>
            <div class="form-group">
                <label for="company">@lang('admin.company')</label>
                <select class="form-control" name="company_id">
                    <option>@lang('admin.select_company')</option>
                    @foreach ($companies as $comany)
                        <option value="{{ $comany->id }}">{{ $comany->name }} </option>
                    @endforeach    
                </select>
            </div>
            <div class="form-group">
                <label for="user_email">@lang('admin.photo')</label>
                <input type="file" name="photo">
            </div>
            <button type="submit" class="btn btn-primary">@lang('admin.submit')</button>
        </form>
    </div>
@endsection