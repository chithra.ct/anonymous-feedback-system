@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.edit_user') }}
                <a class="btn btn-success float-right add_company" href="{{ route('users.create') }}">
                    @lang('admin.users_add') }}
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                @lang('admin.input_error') }}
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{ route('users.update',$user->id) }}" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">@lang('admin.name') }}</label>
                <input type="text" class="form-control" id="user_name" placeholder="@lang('admin.field_error', ['name' => 'name']) }}" name="name" value="{{ old('name', $user->name) }}">
            </div>
            <div class="form-group">
                <label for="email">@lang('admin.email') }}</label>
                <input type="text" class="form-control" id="user_email" placeholder="@lang('admin.field_error', ['name' => 'email']) }}" name="email" value="{{ old('email', $user->email) }}">
            </div>
            <div class="form-group">
                <label for="company">@lang('admin.company') }}</label>
                <select class="form-control" name="company_id">
                    <option>@lang('admin.select_company') }}</option>
                    @foreach ($companies as $comany)
                        <option value="{{ $comany->id }}" {{ ($comany->id == $user->company_id) ? 'selected' : '' }}> 
                            {{ $comany->name }} 
                        </option>
                    @endforeach    
                </select>
            </div>
            <div class="form-group">
                <label for="user_email">@lang('admin.photo') }}</label>
                <input type="file" name="photo">
                @if($user->photo)
                    <img class="user_photo" src="{{asset('/'.config('feedback.path').'/'.$user->photo)}}" alt="{{ $user->photo }}">
                @endif    
            </div>
            <button type="submit" class="btn btn-primary">@lang('admin.submit') }}</button>
        </form>
    </div>
@endsection