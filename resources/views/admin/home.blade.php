@extends('layouts.app')

@section('content')
    <div class="card-header">@lang('admin.dashboard')</div>
    <div class="card-body dashboard">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <a href="{{ url('companies') }}">@lang('admin.manage_company')</a>
        <a href="{{ url('users') }}">@lang('admin.manage_user')</a>
    </div>
@endsection
