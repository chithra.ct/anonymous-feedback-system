@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.companies_title')
                <a class="btn btn-success float-right add_company" href="{{ route('companies.index') }}">
                    @lang('admin.back')
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>@lang('admin.company_name'):</th>
                <td>{{ $company->name }}</td>
            </tr>
            <tr>
                <th>@lang('admin.company_slug'):</th>
                <td>{{ $company->slug }}</td>
            </tr>
        </table>
    </div>
@endsection