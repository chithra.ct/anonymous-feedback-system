@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.companies_title')
                <a class="btn btn-success float-right add_company" href="{{ route('companies.create') }}">
                    @lang('admin.companies_add')
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <table class="table table-bordered">
            <tr>
                <th>@lang('admin.si_no')</th>
                <th>@lang('admin.company_name')</th>
                <th>@lang('admin.company_slug')</th>
                <th>@lang('admin.actions')</th>
            </tr>
            @php
                $i = 0;
            @endphp
            @foreach ($companies as $company)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->slug }}</td>
                    <td>
                        <form action="{{ route('companies.destroy', $company->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('companies.show', $company->id) }}">@lang('admin.show')</a>
                            <a class="btn btn-primary" href="{{ route('companies.edit', $company->id) }}">@lang('admin.edit')</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">@lang('admin.delete')</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $companies->links() !!}
    </div>
@endsection