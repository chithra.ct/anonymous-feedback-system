@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                @lang('admin.edit_company')
                <a class="btn btn-success float-right add_company" href="{{ route('companies.create') }}">
                    @lang('admin.companies_add')
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                @lang('admin.input_error')
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{ route('companies.update',$company->id) }}" >
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="company_name">@lang('admin.company_name')</label>
                <input type="text" class="form-control" id="company_name" placeholder="@lang('admin.field_error', ['name' => 'name'])" name="name" value="{{ $company->name }}">
            </div>
            <div class="form-group">
                <label for="company_slug">@lang('admin.company_slug')</label>
                <input type="text" class="form-control" id="company_slug" placeholder="@lang('admin.field_error', ['name' => 'slug'])" name="slug" value="{{ $company->slug }}">
            </div>
            <button type="submit" class="btn btn-primary">@lang('admin.submit')</button>
        </form>
    </div>
@endsection