<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * Define relationship with users table
     *
     * @author Chithra <cct@lbit.in>
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function user()
    {
    	return $this->hasMany('App\User');
    }
}
