<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'password',
    ];

    /**
     * Define relationship with users table
     *
     * @author Chithra <cct@lbit.in>
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function user()
    {
    	return $this->hasMany('App\User');
    }
}
