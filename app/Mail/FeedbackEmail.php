<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param App\User $user
     * 
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('feedback.subject'))->view('frontend.emails.feedback')->with(['user'=>$this->user]);
    }
}
