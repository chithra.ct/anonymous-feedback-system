<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type_id', 'photo', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * Define relationship with types table
     *
     * @author Chithra <cct@lbit.in>
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function type()
    {
    	return $this->belongsTo('App\Type');
    }
    
    /**
     * Define relationship with companies table
     *
     * @author Chithra <cct@lbit.in>
     * 
     * @return Illuminate\Database\Eloquent\Model
     */
    public function company()
    {
    	return $this->belongsTo('App\Company');
    }
}
