<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the feedback request.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prof_feedback' => 'nullable|max:255',
            'personal_feedback' => 'nullable|max:255',
            'user_id' => 'required',
            'user_email' => 'required|email',
        ];
    }
}
