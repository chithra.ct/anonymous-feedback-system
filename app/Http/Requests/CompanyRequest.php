<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:255',
            'slug' => 'required|min:2|unique:companies|max:20',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $company = $this->route()->parameter('company');

            $rules['name'] = [
                'required',
                'min:2',
                'max:255',
            ];

            $rules['slug'] = [
                'required',
                'min:2',
                'max:255',
                Rule::unique('companies')->ignore($company),
            ];
        }

        return $rules;
    }
}
