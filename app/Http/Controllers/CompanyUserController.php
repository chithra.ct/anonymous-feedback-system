<?php

namespace App\Http\Controllers;

use App\Mail\FeedbackEmail;
use App\Http\Requests\FeedbackRequest;
use App\User;
use App\Company;

class CompanyUserController extends Controller
{
    /**
     * Display a listing of the companies.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param string $slug
     * 
     * @return \Illuminate\Http\Response
     */
    public function company( $slug )
    {
        $company_id = Company::where('slug', $slug)->value('id');
        $users = User::where('type_id', 2)->where('company_id', $company_id)->orderBy('created_at', 'desc')->paginate(10);

        return view('frontend.users', compact('users'));
    }
    /**
     * Display single user.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param App\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function user(User $user)
    { 
        return view('frontend.user', compact('user'));
    }

    /**
     * Send feedback
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  App\Http\Requests\FeedbackRequest  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function feedback(FeedbackRequest $request)
    { 
        if ($request->personal_feedback || $request->prof_feedback) {
            Mail::to($request->get('user_email'))->send(new FeedbackEmail($user));
        }
        return redirect()->route('user', $request->get('user_id'))->with('success', trans('frontend.feedback_success'));
    }
}
