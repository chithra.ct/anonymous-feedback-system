<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Company;
use App\Mail\WelcomeEmail;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type_id', 2)->orderBy('created_at', 'desc')->paginate(10);

        return view('admin.user.list', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all('id', 'name');

        return view('admin.user.create', compact('companies'));
    }

    /**
     * Store a newly created user in storage.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if ($request->get('name') && $request->get('email') && $request->hasFile('photo')) {
            $imageName = time().'.'.$request->photo->extension();
            $request->photo->move(public_path(config('feedback.path')), $imageName);
        }
        $user = new User([
            'name' => $request->get('name'),
            'email'=> $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'type_id' => 2,
            'company_id' => $request->get('company_id') ?? 0,
            'photo' => $imageName?? ''
        ]);
        $user->save();
        Mail::to($request->get('email'))->send(new WelcomeEmail($user));

        return redirect()->route('users.index')->with('success', trans('admin.user_success'));
    }

    /**
     * Display the specified resource.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param App\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.user.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  App\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $companies = Company::all('id', 'name');

        return view('admin.user.edit', compact(['user', 'companies']));
    }

    /**
     * Update the specified user in storage.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @param  App\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        if ($request->get('name') && $request->get('email') && $request->hasFile('photo')) {
            $imageName = time().'.'.$request->photo->extension();
            $request->photo->move(public_path(config('feedback.path')), $imageName);
            if ($oldPhoto = $user->photo) {
                unlink(public_path(config('feedback.path').'/'.$user->photo));
            }
            $user->photo = $imageName?? '';
        }
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->company_id = $request->get('company_id');
        $user->update();

        return redirect()->route('users.index')->with('success',trans('admin.user_update'));
    }

    /**
     * Remove the specified user from storage.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  App\User $user
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
  
        return redirect()->route('users.index')->with('success',trans('admin.user_delete'));
    }
}
