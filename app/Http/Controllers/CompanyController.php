<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\CompanyRequest;

class CompanyController extends Controller
{
    /**
     * Display a listing of the companies.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::orderBy('created_at', 'desc')->paginate(10);

        return view('admin.company.list', compact('companies'));
    }

    /**
     * Show the form for creating a new company.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created company in storage.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  App\Http\Requests\CompanyRequest  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $company = new Company([
            'name' => $request->get('name'),
            'slug'=> $request->get('slug')
        ]);
 
        $company->save();
        
        return redirect()->route('company.index')->with('success', trans('admin.company_success'));
    }

    /**
     * Display the specified company.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  \App\Company  $company
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return view('admin.company.view',compact('company'));
    }

    /**
     * Show the form for editing the specified company.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  \App\Company  $company
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.company.edit',compact('company'));
    }

    /**
     * Update the specified company in storage.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  App\Http\Requests\CompanyRequest $request
     * @param  \App\Company  $company
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $company->update($request->all());
  
        return redirect()->route('company.index')->with('success',trans('admin.company_update'));
    }

    /**
     * Remove the specified company from storage.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @param  int  $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
  
        return redirect()->route('company.index')->with('success',trans('admin.company_delete'));
    }
}
