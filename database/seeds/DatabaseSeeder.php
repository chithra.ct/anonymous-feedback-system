<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TypesTableSeeder::class,
            UsersTableSeeder::class
        ]);
    }
}
