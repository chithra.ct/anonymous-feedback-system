<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @author Chithra<cct@lbit.in>
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name' => 'Administrator',
            'created_at' => date("Y-m-d H:i:s")
        ]);
        DB::table('types')->insert([
            'name' => 'User',
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
